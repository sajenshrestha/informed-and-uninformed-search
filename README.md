# Informed and Uninformed Search

The program will compute a route between the origin city and the destination city, and will print out both the length of the route and the list of all cities that lie on that route. It should also display the number of nodes expanded. For example,

find_route input1.txt Bremen Kassel

should have the following output:

nodes expanded: 12
distance: 297 km
route: 
Bremen to Hannover, 132 km 
Hannover to Kassel, 165 km 

and

find_route input1.txt London Kassel

should have the following output:

nodes expanded: 7
distance: infinity
route: 
none